package com.kotlin.aria.futureworkshopsapp.domain.base.fetcher.result_listener

/**
 * Created by aria on 28/12/2017.
 */
enum class RequestType {
    TYPE_NONE,
    NEWS_LIST,
    NEWS_DETAILS
}