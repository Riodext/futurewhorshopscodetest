package com.kotlin.aria.futureworkshopsapp.presentation.login.presenter

import com.kotlin.aria.futureworkshopsapp.presentation.base.BasePresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.login.contract.LoginContract
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class LoginPresenter @Inject constructor()
    : BasePresenter<LoginContract.View>(), LoginContract.Presenter {

    override fun onPresenterCreate() {
        super.onPresenterCreate()
        view?.initUI()
    }

    override fun login(username: String) {
        //In a real production app this should call a login service.
        if (username.isEmpty()) {
            view?.showNoInputUI()
        } else {
            view?.hideTheKeyboard()
            view?.saveUserName(username)
            view?.onLogin()
        }
    }

}