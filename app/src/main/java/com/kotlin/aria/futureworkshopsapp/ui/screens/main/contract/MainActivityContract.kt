package com.kotlin.aria.futureworkshopsapp.ui.screens.main.contract

import android.support.v4.app.Fragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseContract

/**
 * Created by aria on 09/12/2017.
 */
interface MainActivityContract {

    interface View: BaseContract.View {

        fun setUpFragment()

        fun updateToolbarTitle(title: String)

        fun removeUser()

        fun goToLoginScreen()
    }

    interface Presenter: BaseContract.Presenter<View> {

        fun onFragmentChanged(currentFragmentTag: String, fragment: Fragment)

        fun logout()
    }
}