package com.kotlin.aria.futureworkshopsapp.presentation.newslist.model

/**
 * Created by aria on 28/12/2017.
 */
data class PresentationNews (
        val id: String,
        val title: String,
        val iconUrl: String,
        val summary: String,
        val date: String,
        val content: String)