package com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.*
import android.widget.Toast
import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseFragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.ImageLoaderContract
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.model.PresentationNews
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.presenter.NewsListPresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.fragment.NewsDetailsFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.adapter.NewsListAdapter
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.adapter.listener.NewsClickListener
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.contract.NewsListContract
import com.kotlin.aria.futureworkshopsapp.ui.utils.constants.BundleConstants
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.gone
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.sethasFixedSizeToFalse
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.visible
import com.kotlin.aria.futureworkshopsapp.ui.utils.preferences.PreferencesUtil
import com.kotlin.aria.futureworkshopsapp.ui.utils.widgets.DividerItemDecorationRecyclerView
import kotlinx.android.synthetic.main.fragment_news_list.*
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class NewsListFragment: BaseFragment<NewsListContract.View, NewsListContract.Presenter>(),
        NewsListContract.View, NewsClickListener {

    @Inject lateinit var newsListPresenter: NewsListPresenter

    @Inject lateinit var imageLoader: ImageLoaderContract

    @Inject lateinit var preferences: PreferencesUtil

    private var listAdapter: NewsListAdapter? = null

    override val layoutResId = R.layout.fragment_news_list

    override fun initPresenter() = newsListPresenter

    override fun injectDependecies() {
        activityComponent.inject(this)
    }

    override fun showLoading() {
        progress_bar?.visible()
    }

    override fun hideLoading() {
        progress_bar?.gone()
    }

    private infix fun setupRecyclerView(list: List<PresentationNews>) {
        listAdapter = NewsListAdapter(list, imageLoader, this)
        rv_news_list.layoutManager = GridLayoutManager(activity, 1)
        rv_news_list.sethasFixedSizeToFalse()
        rv_news_list.adapter = listAdapter
        rv_news_list.scheduleLayoutAnimation()

        rv_news_list.addItemDecoration(DividerItemDecorationRecyclerView(activity))
    }

    override fun onItemsReceive(list: List<PresentationNews>) {
        rv_news_list.visible()
        listAdapter?.update(list) ?: this setupRecyclerView list
    }

    override fun showNoItemsUI() {
        rv_news_list.gone()
        tv_no_items.visible()
    }

    override fun hideNoItemsUI() {
        tv_no_items.gone()
    }

    override fun showError() {
        Toast.makeText(activity, getString(R.string.error_news_list), Toast.LENGTH_SHORT).show()
    }

    override fun onItemClick(presentationNews: PresentationNews) {
        goTo<NewsDetailsFragment>(
                false,
                true,
                Bundle().apply {
                    putString(BundleConstants.NEWS_ID, presentationNews.id)
                    putString(BundleConstants.NEWS_TITLE, presentationNews.title)
                })
    }

    override fun getToolbarTitle() =
            String.format(getString(R.string.newslist_title), preferences.username)

    override fun onResume() {
        super.onResume()
        activity.invalidateOptionsMenu()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        activity.getMenuInflater().inflate(R.menu.menu_newslist, menu)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        setHasOptionsMenu(true)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    override fun onPrepareOptionsMenu(menu: Menu?) {
        super.onPrepareOptionsMenu(menu)
        menu?.clear()
        activity.getMenuInflater().inflate(R.menu.menu_newslist, menu)
    }

}