package com.kotlin.aria.futureworkshopsapp.data.newslist.repositoryimp.network

import com.kotlin.aria.futureworkshopsapp.data.newslist.mapper.NewsListResponseMapper
import com.kotlin.aria.futureworkshopsapp.data.newslist.repositoryimp.network.service.NewsListService
import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews
import com.kotlin.aria.futureworkshopsapp.domain.search.repository.NewsListRepository
import io.reactivex.Flowable

/**
 * Created by aria on 28/12/2017.
 */
class NewsListRepositoryNetworkImp constructor(
        private val newsListService: NewsListService,
        private val newsListMapper: NewsListResponseMapper): NewsListRepository {

    override fun fetchNews(): Flowable<List<DomainNews>> {
        return newsListService.fetchNewsList().map {
            newsListMapper.translate(it)
        }
    }

}