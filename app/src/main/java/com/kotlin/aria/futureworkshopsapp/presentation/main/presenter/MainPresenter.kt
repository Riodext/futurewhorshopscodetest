package com.kotlin.aria.futureworkshopsapp.presentation.main.presenter

import android.support.v4.app.Fragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseFragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.BasePresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.contract.MainActivityContract
import javax.inject.Inject

/**
 * Created by aria on 09/12/2017.
 */
class MainPresenter @Inject constructor()
    : BasePresenter<MainActivityContract.View>(), MainActivityContract.Presenter {

    override fun onPresenterCreate() {
        super.onPresenterCreate()
        view?.setUpFragment()
    }

    override fun onFragmentChanged(currentFragmentTag: String, fragment: Fragment) {
        if (fragment is BaseFragment<*, *>) {
            view?.updateToolbarTitle(fragment.getToolbarTitle())
        }
    }

    override fun logout() {
        view?.removeUser()
        view?.goToLoginScreen()
    }
}