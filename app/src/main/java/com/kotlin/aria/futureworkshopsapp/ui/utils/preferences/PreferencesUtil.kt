package com.kotlin.aria.futureworkshopsapp.ui.utils.preferences

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.emptyString
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.unSafeLazy
import javax.inject.Inject

/**
 * Created by aria on 29/12/2017.
 */
class PreferencesUtil @Inject constructor(app: Application) {

    private val SHARED_PREF_NAME = R.string.shared_preferences_name
    private val PREF_USERNAME = "user_logged_in"

    private val sharedPreferences by unSafeLazy {
        app.getSharedPreferences(
                app.applicationContext.getString(SHARED_PREF_NAME), Context.MODE_PRIVATE)
    }

    val isUserLoggedIn
        get() = sharedPreferences.contains(PREF_USERNAME)

    fun saveUserName(username: String) {
        sharedPreferences.put { putString(PREF_USERNAME, username) }
    }

    fun saveUserLogout() {
        sharedPreferences.put { remove(PREF_USERNAME) }
    }

    val username: String
        get() = sharedPreferences.getString(PREF_USERNAME, emptyString)

    private inline fun SharedPreferences.put(body: SharedPreferences.Editor.() -> Unit) {
        val editor = this.edit()
        editor.body()
        editor.apply()
    }
}