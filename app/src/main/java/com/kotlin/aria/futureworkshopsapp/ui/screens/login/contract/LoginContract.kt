package com.kotlin.aria.futureworkshopsapp.ui.screens.login.contract

import com.kotlin.aria.futureworkshopsapp.presentation.base.ApiContract

/**
 * Created by aria on 28/12/2017.
 */
interface LoginContract {

    interface View: ApiContract.View {

        fun initUI()

        fun onLogin()

        fun saveUserName(username: String)

        fun showNoInputUI()

        fun hideTheKeyboard()
    }

    interface Presenter: ApiContract.Presenter<View> {

        fun login(username: String)
    }
}