package com.kotlin.aria.futureworkshopsapp.data.newsdetails.repositoryimp.network.service

import com.kotlin.aria.futureworkshopsapp.data.newslist.model.NewsDetailsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by aria on 28/12/2017.
 */
interface NewsDetailsService {

    @GET("{id}.json")
    fun fetchNewsDetails(
            @Path("id") id: String): Single<NewsDetailsResponse>
}