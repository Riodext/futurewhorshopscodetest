package com.kotlin.aria.futureworkshopsapp.domain.search.repository

import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews
import io.reactivex.Flowable

/**
 * Created by aria on 26/12/2017.
 */
interface NewsListRepository {

    fun fetchNews(): Flowable<List<DomainNews>>
}