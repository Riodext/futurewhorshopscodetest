package com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.adapter

import android.view.View
import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseRecyclerViewAdapter
import com.kotlin.aria.futureworkshopsapp.presentation.base.ImageLoaderContract
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.model.PresentationNews
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.adapter.listener.NewsClickListener
import kotlinx.android.synthetic.main.newslist_item.view.*

/**
 * Created by aria on 28/12/2017.
 */
class NewsListAdapter constructor(
        list: List<PresentationNews>,
        private val imageLoader: ImageLoaderContract,
        private val newsClickListener: NewsClickListener)
    : BaseRecyclerViewAdapter<PresentationNews>(list, R.layout.newslist_item) {

    override fun onItemClick(itemView: View, position: Int) {
        newsClickListener.onItemClick(itemList[position])
    }

    override fun View.bind(item: PresentationNews) {
        tv_item_date.text = item.date
        tv_item_title.text = item.title
        tv_item_summary.text = item.summary
        imageLoader.load(context, item.iconUrl, iv_search_item_picture)
    }

}