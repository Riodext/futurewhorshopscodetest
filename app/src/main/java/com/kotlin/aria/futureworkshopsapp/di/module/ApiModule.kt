package com.kotlin.aria.futureworkshopsapp.di.module

import com.kotlin.aria.futureworkshopsapp.BuildConfig
import com.kotlin.aria.futureworkshopsapp.data.ApiConstants
import com.kotlin.aria.futureworkshopsapp.data.newsdetails.mapper.NewsDetailsResponseMapper
import com.kotlin.aria.futureworkshopsapp.data.newsdetails.repositoryimp.network.NewsDetailsRepositoryImp
import com.kotlin.aria.futureworkshopsapp.data.newsdetails.repositoryimp.network.service.NewsDetailsService
import com.kotlin.aria.futureworkshopsapp.data.newslist.mapper.NewsListResponseMapper
import com.kotlin.aria.futureworkshopsapp.data.newslist.repositoryimp.network.NewsListRepositoryNetworkImp
import com.kotlin.aria.futureworkshopsapp.data.newslist.repositoryimp.network.service.NewsListService
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.repository.NewsDetailsRepository
import com.kotlin.aria.futureworkshopsapp.domain.search.repository.NewsListRepository
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton


/**
 * Created by aria on 03/12/2017.
 */
@Module
class ApiModule {

    @Singleton
    @Provides
    fun provideCompositeDisposable() = CompositeDisposable()

    @Singleton
    @Provides
    @Named("baseEndpoint")
    fun baseEndpoint() = ApiConstants.BASE_ENDPOINT

    @Singleton
    @Provides
    fun provideOkHttpBuilder(): OkHttpClient.Builder {
        val okHttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BASIC
            okHttpBuilder.addInterceptor(logging)
        }
        okHttpBuilder.readTimeout(15.toLong(), TimeUnit.SECONDS)
        okHttpBuilder.connectTimeout(15.toLong(), TimeUnit.SECONDS)

        return okHttpBuilder
    }

    @Singleton
    @Provides
    fun provideRetrofit(retrofitBuilder: Retrofit.Builder,
                        okHttpClientBuilder: OkHttpClient.Builder,
                        @Named("baseEndpoint") baseUrl: String): Retrofit {
        val client = okHttpClientBuilder.build()
        return retrofitBuilder
                .client(client)
                .baseUrl(baseUrl)
                .build()
    }

    @Singleton
    @Provides
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
    }

    @Singleton
    @Provides
    fun provideNewsListRepositoryImp(
            newsListService: NewsListService,
            newsListMapper: NewsListResponseMapper): NewsListRepository =
                NewsListRepositoryNetworkImp(newsListService, newsListMapper)

    @Singleton
    @Provides
    fun provideNewsDetailsService(retrofit: Retrofit): NewsDetailsService =
            retrofit.create(NewsDetailsService::class.java)

    @Singleton
    @Provides
    fun provideNewsListService(retrofit: Retrofit): NewsListService =
            retrofit.create(NewsListService::class.java)

    @Singleton
    @Provides
    fun providesNewsListMapper(): NewsListResponseMapper = NewsListResponseMapper()

    @Singleton
    @Provides
    fun providesNewsDetailsRepositoryImp(
            service: NewsDetailsService, mapper: NewsDetailsResponseMapper)
            : NewsDetailsRepository =
                NewsDetailsRepositoryImp(service, mapper)

    @Singleton
    @Provides
    fun providesNewsDetailsMapper(): NewsDetailsResponseMapper = NewsDetailsResponseMapper()

}