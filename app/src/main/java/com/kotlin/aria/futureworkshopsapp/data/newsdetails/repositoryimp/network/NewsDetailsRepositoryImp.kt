package com.kotlin.aria.futureworkshopsapp.data.newsdetails.repositoryimp.network

import com.kotlin.aria.futureworkshopsapp.data.newsdetails.mapper.NewsDetailsResponseMapper
import com.kotlin.aria.futureworkshopsapp.data.newsdetails.repositoryimp.network.service.NewsDetailsService
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity.DomainNewsDetails
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.repository.NewsDetailsRepository
import io.reactivex.Single

/**
 * Created by aria on 28/12/2017.
 */
class NewsDetailsRepositoryImp constructor(
        private val service: NewsDetailsService,
        private val mapper: NewsDetailsResponseMapper): NewsDetailsRepository{

    override fun fetchDetails(id: String): Single<DomainNewsDetails> {
        return service.fetchNewsDetails(id).map {
            mapper.translate(it) }
    }

}