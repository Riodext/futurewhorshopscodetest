package com.kotlin.aria.futureworkshopsapp.data.newslist.repositoryimp.network.service

import com.kotlin.aria.futureworkshopsapp.data.newslist.model.NewsListResponse
import io.reactivex.Flowable
import retrofit2.http.GET

/**
 * Created by aria on 28/12/2017.
 */
interface NewsListService {

    @GET("fw-coding-test.json")
    fun fetchNewsList(): Flowable<NewsListResponse>
}