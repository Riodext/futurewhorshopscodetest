package com.kotlin.aria.futureworkshopsapp.data.newslist.mapper

import com.kotlin.aria.futureworkshopsapp.data.newslist.model.NewsListResponse
import com.kotlin.aria.futureworkshopsapp.data.newslist.model.NewsResponse
import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews

/**
 * Created by aria on 26/12/2017.
 */
class NewsListResponseMapper {

    //think doing this mapper in Java ;)
    fun translate(list: NewsListResponse): List<DomainNews> {
        return list.items.asSequence()
                .map { translate(it) }
                .toList()
    }

    private fun translate(newsResponse: NewsResponse): DomainNews {
        return DomainNews(
                newsResponse.id,
                newsResponse.title,
                newsResponse.iconUrl,
                newsResponse.summary,
                newsResponse.date,
                newsResponse.content)
    }
}