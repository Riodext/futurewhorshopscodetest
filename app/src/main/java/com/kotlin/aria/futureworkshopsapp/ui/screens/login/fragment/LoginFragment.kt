package com.kotlin.aria.futureworkshopsapp.ui.screens.login.fragment

import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseFragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.ImageLoaderContract
import com.kotlin.aria.futureworkshopsapp.presentation.login.presenter.LoginPresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.login.contract.LoginContract
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.hideKeyboard
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.showToast
import com.kotlin.aria.futureworkshopsapp.ui.utils.preferences.PreferencesUtil
import kotlinx.android.synthetic.main.fragment_login.*
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class LoginFragment: BaseFragment<LoginContract.View, LoginContract.Presenter>(),
        LoginContract.View {

    @Inject lateinit var loginPresenter: LoginPresenter

    @Inject lateinit var imageLoader: ImageLoaderContract

    @Inject lateinit var preferences: PreferencesUtil

    override val layoutResId = R.layout.fragment_login

    override fun initPresenter() = loginPresenter

    override fun injectDependecies() {
        activityComponent.inject(this)
    }

    override fun showLoading() {
        // TODO add some fancy UI
    }

    override fun hideLoading() {
        // TODO
    }

    override fun showError() {
        // TODO
    }

    override fun onLogin() {
        goTo<NewsListFragment>()
    }

    override fun initUI() {
        showHeaderImage()
        setLoginClick()
    }

    private fun setLoginClick() {
        btn_login.setOnClickListener {
            loginPresenter.login(et_future_username_workshops.text.toString())
        }
    }

    private fun showHeaderImage() {
        imageLoader.load(
                context,
                "https://s3.amazonaws.com/future-workshops/fw-coding-login-image.jpg",
                iv_login_header)
    }

    override fun saveUserName(username: String) {
        preferences.saveUserName(username)
    }

    override fun showNoInputUI() {
        context.showToast(R.string.minim_characters_error)
    }

    override fun hideTheKeyboard() {
        et_future_username_workshops.hideKeyboard()
    }

    override fun getToolbarTitle() =
            getString(R.string.login_title)

}