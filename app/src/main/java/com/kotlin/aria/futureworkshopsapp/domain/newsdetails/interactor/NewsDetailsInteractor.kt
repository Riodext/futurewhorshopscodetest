package com.kotlin.aria.futureworkshopsapp.domain.newsdetails.interactor

import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity.DomainNewsDetails
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.repository.NewsDetailsRepository
import io.reactivex.Single

/**
 * Created by aria on 28/12/2017.
 */
class NewsDetailsInteractor constructor(
        private val repository: NewsDetailsRepository) {

    fun fetchNewsDetails(id: String): Single<DomainNewsDetails> {
        return repository.fetchDetails(id)
    }
}