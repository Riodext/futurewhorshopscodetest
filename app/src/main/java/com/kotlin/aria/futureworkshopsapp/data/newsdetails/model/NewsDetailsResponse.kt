package com.kotlin.aria.futureworkshopsapp.data.newslist.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by aria on 28/12/2017.
 *
 * {
    id: 100,
    title: "'iPhone 8' to Come in Four Colors Including New 'Mirror-Like' Option",
    image_url: "https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl.jpg-large-800x600.jpeg",
    source: "macrumors",
    content: "Apple could make its upcoming OLED iPhone available in four different shades, including a new mirror-like reflective version not seen before in previous models. That's the latest claim from mobile leaker Benjamin Geskin, who shared an example image via Twitter over the weekend showing what the new color option could resemble.",
    date: "11/12/2016"
 * }
 *
 * https://s3.amazonaws.com/future-workshops/100.json
 *
 */
data class NewsDetailsResponse(
        @SerializedName("id")
        @Expose
        val id: String,
        @SerializedName("title")
        @Expose
        val title: String,
        @SerializedName("image_url")
        @Expose
        val imageUrl: String,
        @SerializedName("source")
        @Expose
        val source: String,
        @SerializedName("content")
        @Expose
        val content: String,
        @SerializedName("date")
        @Expose
        val date: String)
