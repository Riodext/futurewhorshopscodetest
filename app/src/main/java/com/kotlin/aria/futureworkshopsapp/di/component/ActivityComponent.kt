package com.kotlin.aria.futureworkshopsapp.di.component

import com.kotlin.aria.futureworkshopsapp.di.module.ActivityModule
import com.kotlin.aria.futureworkshopsapp.di.scope.PerActivity
import com.kotlin.aria.futureworkshopsapp.ui.screens.login.fragment.LoginFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.activity.MainActivity
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.fragment.NewsDetailsFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment
import dagger.Subcomponent

/**
 * Created by aria on 06/12/2017.
 */
@PerActivity
@Subcomponent(modules = [(ActivityModule::class)])

interface ActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(newsListFragment: NewsListFragment)

    fun inject(newsDetailsFragment: NewsDetailsFragment)

    fun inject(loginFragment: LoginFragment)

}