package com.kotlin.aria.futureworkshopsapp.domain.newsdetails.repository

import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity.DomainNewsDetails
import io.reactivex.Single

/**
 * Created by aria on 28/12/2017.
 */
interface NewsDetailsRepository {

    fun fetchDetails(id: String): Single<DomainNewsDetails>
}