package com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity

/**
 * Created by aria on 28/12/2017.
 */
data class DomainNewsDetails(
        val id: String,
        val title: String,
        val imageUrl: String,
        val source: String,
        val content: String,
        val date: String)
