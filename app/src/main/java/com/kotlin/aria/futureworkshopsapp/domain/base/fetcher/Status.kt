package com.kotlin.aria.futureworkshopsapp.domain.fetcher

/**
 * Created by aria on 03/12/2017.
 */
sealed class Status {

    object LOADING : Status()
    object ERROR : Status()
    object SUCCESS : Status()
    object EMPTY_SUCCESS : Status()
    object IDLE : Status()
}