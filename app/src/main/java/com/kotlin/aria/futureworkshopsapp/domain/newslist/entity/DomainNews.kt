package com.kotlin.aria.futureworkshopsapp.domain.search.entity

/**
 * Created by aria on 26/12/2017.
 */
data class DomainNews (
    val id: String,
    val title: String,
    val iconUrl: String,
    val summary: String,
    val date: String,
    val content: String)