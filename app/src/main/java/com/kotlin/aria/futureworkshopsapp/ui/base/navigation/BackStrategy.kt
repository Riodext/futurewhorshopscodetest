package com.kotlin.aria.futureworkshopsapp.presentation.navigation


import io.reactivex.annotations.Experimental

/**
 * Created by aria on 09/12/2017.
 */
@Experimental
sealed class BackStrategy {

    object KEEP : BackStrategy()
    object DESTROY : BackStrategy()
}