package com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.presenter

import android.support.annotation.CallSuper
import com.kotlin.aria.futureworkshopsapp.domain.base.fetcher.result_listener.RequestType
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.interactor.NewsDetailsInteractor
import com.kotlin.aria.futureworkshopsapp.presentation.base.ApiFetcherPresenter
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.mapper.PresentationNewsDetailsMapper
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.contract.NewsDetailsContract
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class NewsDetailsPresenter @Inject constructor(
        private val interactor: NewsDetailsInteractor,
        private val mapper: PresentationNewsDetailsMapper):
        ApiFetcherPresenter<NewsDetailsContract.View>(), NewsDetailsContract.Presenter {

    @CallSuper
    override fun onPresenterCreate() {
        super.onPresenterCreate()
        view?.initUI()
    }

    override fun onRequestError(errorMessage: String?) {
        view?.hideLoading()
        view?.showError()
    }

    override fun onRequestStart() {
        view?.showLoading()
    }

    override fun getNewsDetails(id: String) {
        fetch(interactor.fetchNewsDetails(id), RequestType.NEWS_DETAILS) {
            view?.onDetailsReceive(mapper.translate(it))
        }
    }

}