package com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.model

/**
 * Created by aria on 28/12/2017.
 */
data class PresentationNewsDetails (
        val id: String,
        val title: String,
        val imageUrl: String,
        val source: String,
        val content: String,
        val date: String)