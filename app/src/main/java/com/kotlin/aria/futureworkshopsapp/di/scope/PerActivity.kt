package com.kotlin.aria.futureworkshopsapp.di.scope

import javax.inject.Scope

/**
 * Created by aria on 03/12/2017.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity