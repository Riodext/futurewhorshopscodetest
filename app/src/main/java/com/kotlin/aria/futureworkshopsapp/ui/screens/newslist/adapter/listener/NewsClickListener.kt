package com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.adapter.listener

import com.kotlin.aria.futureworkshopsapp.presentation.newslist.model.PresentationNews

/**
 * Created by aria on 28/12/2017.
 */
interface NewsClickListener {

    fun onItemClick(presentationNews: PresentationNews)
}