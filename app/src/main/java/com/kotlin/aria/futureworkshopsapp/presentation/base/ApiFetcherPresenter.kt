package com.kotlin.aria.futureworkshopsapp.presentation.base

import android.support.annotation.CallSuper
import com.kotlin.aria.futureworkshopsapp.domain.base.fetcher.result_listener.RequestType
import com.kotlin.aria.futureworkshopsapp.domain.fetcher.Fetcher
import com.kotlin.aria.futureworkshopsapp.domain.fetcher.result_listener.ResultListener
import io.reactivex.Flowable
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by aria on 06/12/2017.
 */
abstract class ApiFetcherPresenter<VIEW : BaseContract.View> : BasePresenter<VIEW>(), ResultListener {

    @Inject
    protected lateinit var fetcher: Fetcher

    fun <TYPE> fetch(flowable: Flowable<TYPE>,
                     requestType: RequestType = RequestType.TYPE_NONE, success: (TYPE) -> Unit) {
        fetcher.fetch(flowable, requestType, this, success)
    }

    fun <TYPE> fetch(single: Single<TYPE>,
                     requestType: RequestType = RequestType.TYPE_NONE, success: (TYPE) -> Unit) {
        fetcher.fetch(single, requestType, this, success)
    }

    override fun onRequestStart(requestType: RequestType) {
        onRequestStart()
    }

    override fun onRequestError(requestType: RequestType, errorMessage: String?) {
        onRequestError(errorMessage)
    }

    @CallSuper
    override fun onPresenterDestroy() {
        super.onPresenterDestroy()
        fetcher.clear()
    }
}