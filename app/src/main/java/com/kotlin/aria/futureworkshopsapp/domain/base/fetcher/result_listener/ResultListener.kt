package com.kotlin.aria.futureworkshopsapp.domain.fetcher.result_listener

import com.kotlin.aria.futureworkshopsapp.domain.base.fetcher.result_listener.RequestType

/**
 * Created by aria on 03/12/2017.
 */
interface ResultListener {

    fun onRequestStart()

    fun onRequestStart(requestType: RequestType)

    fun onRequestError(errorMessage: String?)

    fun onRequestError(requestType: RequestType, errorMessage: String?)
}