package com.kotlin.aria.futureworkshopsapp.presentation.base

/**
 * Created by aria on 07/12/2017.
 */
interface ApiContract {

    interface View: BaseContract.View {

        fun showLoading()

        fun hideLoading()

        fun showError()
    }

    interface Presenter<V: BaseContract.View> : BaseContract.Presenter<V>
}