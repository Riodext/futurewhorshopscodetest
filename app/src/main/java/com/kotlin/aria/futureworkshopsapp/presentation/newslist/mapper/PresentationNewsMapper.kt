package com.kotlin.aria.futureworkshopsapp.presentation.newslist.mapper

import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.model.PresentationNews

/**
 * Created by aria on 28/12/2017.
 */
class PresentationNewsMapper {

    //think doing this mapper in Java ;)
    fun translate(list: List<DomainNews>): List<PresentationNews> {
        return list.asSequence()
                .map { translate(it) }
                .toList()
    }

    private fun translate(domainNews: DomainNews): PresentationNews {
        return PresentationNews(
                domainNews.id,
                domainNews.title,
                domainNews.iconUrl,
                domainNews.summary,
                domainNews.date,
                domainNews.content)
    }

}