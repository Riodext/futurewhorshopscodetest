package com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.contract

import com.kotlin.aria.futureworkshopsapp.presentation.base.ApiContract
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.model.PresentationNews

/**
 * Created by aria on 28/12/2017.
 */
interface NewsListContract {

    interface View: ApiContract.View {

        fun onItemsReceive(list: List<PresentationNews>)

        fun showNoItemsUI()

        fun hideNoItemsUI()
    }

    interface Presenter : ApiContract.Presenter<View> {

    }
}