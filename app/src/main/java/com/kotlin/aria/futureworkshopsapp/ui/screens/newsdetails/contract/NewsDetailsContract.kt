package com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.contract

import com.kotlin.aria.futureworkshopsapp.presentation.base.ApiContract
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.model.PresentationNewsDetails

/**
 * Created by aria on 28/12/2017.
 */
interface NewsDetailsContract {

    interface View: ApiContract.View {

        fun initUI()

        fun onDetailsReceive(presentationNewsDetails: PresentationNewsDetails)
    }

    interface Presenter: ApiContract.Presenter<View> {

        fun getNewsDetails(id: String)
    }
}