package com.kotlin.aria.futureworkshopsapp.data.newslist.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by aria on 28/12/2017.
 *
 * {
        articles: [
        {
        id: 100,
        title: "'iPhone 8' to Come in Four Colors Including New 'Mirror-Like' Option",
        icon_url: "https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl.jpg-large-800x600.jpeg",
        summary: "Apple could make its upcoming OLED iPhone available in four different shades, including a new mirror-like reflective version not seen before in previous models. That's the latest claim from mobile leaker Benjamin Geskin, who shared an example image via Twitter over the weekend showing what the new color option could resemble.",
        date: "11/12/2016",
        content: "https://s3.amazonaws.com/future-workshops/100.json"
        },
        {
        id: 101,
        title: "Wireless Charging Accessory Might Not Ship Until After New 2017 iPhones Launch",
        summary: "Apple's upcoming 2017 iPhone lineup is expected to include an inductive wireless charging feature enabled through a standalone charging accessory, and new information shared by Apple blogger John Gruber suggests the accessory might not ship alongside the iPhones in September. ",
        icon_url: "https://cdn.macrumors.com/article-new/2017/02/qi-charging.jpg",
        date: "02/05/2017",
        content: "https://s3.amazonaws.com/future-workshops/101.json"
        }
        ],
 *
 * https://s3.amazonaws.com/future-workshops/fw-coding-test.json
 *
 */
data class NewsListResponse(
        @SerializedName("articles")
        @Expose
        val items: List<NewsResponse>)

data class NewsResponse (
        @SerializedName("id")
        @Expose
        val id: String,
        @SerializedName("title")
        @Expose
        val title: String,
        @SerializedName("icon_url")
        @Expose
        val iconUrl: String,
        @SerializedName("summary")
        @Expose
        val summary: String,
        @SerializedName("date")
        @Expose
        val date: String,
        @SerializedName("content")
        @Expose
        val content: String)
