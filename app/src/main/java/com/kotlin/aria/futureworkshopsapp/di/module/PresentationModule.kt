package com.kotlin.aria.futureworkshopsapp.di.module

import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.mapper.PresentationNewsDetailsMapper
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.mapper.PresentationNewsMapper
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by aria on 26/12/2017.
 */
@Module
class PresentationModule {

    @Singleton
    @Provides
    fun providesPresentationNewsMapper(): PresentationNewsMapper =
            PresentationNewsMapper()

    @Singleton
    @Provides
    fun providesPresentationNewsDetailsMapper(): PresentationNewsDetailsMapper =
            PresentationNewsDetailsMapper()
}