package com.kotlin.aria.futureworkshopsapp

import android.app.Application
import com.kotlin.aria.futureworkshopsapp.di.component.ApplicationComponent
import com.kotlin.aria.futureworkshopsapp.di.component.DaggerApplicationComponent
import com.kotlin.aria.futureworkshopsapp.di.module.ApplicationModule
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.unSafeLazy


/**
 * Created by aria on 06/12/2017.
 */
class App: Application() {

    val applicationComponent: ApplicationComponent by unSafeLazy {
        DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

}