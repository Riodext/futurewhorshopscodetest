package com.kotlin.aria.futureworkshopsapp.di.module

import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import com.kotlin.aria.futureworkshopsapp.di.scope.PerActivity
import com.kotlin.aria.futureworkshopsapp.presentation.base.GlideImageLoader
import com.kotlin.aria.futureworkshopsapp.presentation.base.ImageLoaderContract
import dagger.Module
import dagger.Provides

/**
 * Created by aria on 06/12/2017.
 */
@Module
class ActivityModule(private val activity: AppCompatActivity) {

    @PerActivity
    @Provides
    fun providesActivity(): AppCompatActivity = activity

    @PerActivity
    @Provides
    fun providesFragmentManager(activity: AppCompatActivity): FragmentManager =
            activity.supportFragmentManager

    @PerActivity
    @Provides
    fun providesImageLoader(): ImageLoaderContract = GlideImageLoader() // could be Picasso or other

}