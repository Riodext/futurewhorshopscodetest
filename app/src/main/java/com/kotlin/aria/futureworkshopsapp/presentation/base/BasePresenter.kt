package com.kotlin.aria.futureworkshopsapp.presentation.base

import com.kotlin.aria.basemvp.BaseMVPPresenter

/**
 * Created by aria on 07/12/2017.
 */
abstract class  BasePresenter<V : BaseContract.View> : BaseMVPPresenter<V>(), BaseContract.Presenter<V>