package com.kotlin.aria.futureworkshopsapp.ui.utils.constants

/**
 * Created by aria on 28/12/2017.
 */
object BundleConstants {

    const val NEWS_ID = "news_id"
    const val NEWS_TITLE = "news_title"
}