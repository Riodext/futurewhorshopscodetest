package com.kotlin.aria.futureworkshopsapp.presentation.base

import android.os.Bundle
import android.support.annotation.CallSuper
import android.support.v4.app.Fragment
import com.kotlin.aria.basemvp.BaseMVPActivity
import com.kotlin.aria.futureworkshopsapp.App
import com.kotlin.aria.futureworkshopsapp.di.component.ActivityComponent
import com.kotlin.aria.futureworkshopsapp.di.module.ActivityModule
import com.kotlin.aria.futureworkshopsapp.presentation.navigation.Navigator
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.unSafeLazy
import javax.inject.Inject

/**
 * Created by aria on 06/12/2017.
 */
abstract class BaseActivity<V: BaseContract.View, P: BaseContract.Presenter<V>>
    : BaseMVPActivity<V, P>(), Navigator.FragmentChangeListener {

    @Inject
    lateinit var navigator: Navigator

    val activityComponent: ActivityComponent by unSafeLazy {
        getAppComponent() + ActivityModule(this)
    }

    private fun getAppComponent() = App.instance.applicationComponent

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        navigator.fragmentChangeListener = this
        super.onCreate(savedInstanceState)
    }

    inline protected fun <reified T : Fragment> goTo(keepState: Boolean = true,
                                                     withCustomAnimation: Boolean = false,
                                                     arg: Bundle = Bundle.EMPTY) {
        navigator.goTo<T>(keepState = keepState,
                withCustomAnimation = withCustomAnimation,
                arg = arg)
    }

    protected abstract fun injectDependencies()

    override fun onBackPressed() {
        if (navigator.hasBackStack()) navigator.goBack()
        else super.onBackPressed()
    }
}