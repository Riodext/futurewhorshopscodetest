package com.kotlin.aria.futureworkshopsapp.presentation.base

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kotlin.aria.basemvp.BaseMVPFragment
import com.kotlin.aria.futureworkshopsapp.di.component.ActivityComponent
import com.kotlin.aria.futureworkshopsapp.presentation.navigation.Navigator
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.emptyString
import javax.inject.Inject

/**
 * Created by aria on 09/12/2017.
 */
abstract class BaseFragment<V: BaseContract.View, P: BaseContract.Presenter<V>>
    : BaseMVPFragment<V, P>() {

    @Inject lateinit var navigator: Navigator

    protected lateinit var activityComponent: ActivityComponent
    protected lateinit var activity: BaseActivity<*, *>

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (context is BaseActivity<*, *>).let {
            this.activity = context as BaseActivity<*, *>
            activityComponent = activity.activityComponent
            injectDependecies()
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(layoutResId, container, false)
    }

    inline fun <reified T : Fragment> goTo(keepState: Boolean = true,
                                           withCustomAnimation: Boolean = false,
                                           arg: Bundle = Bundle.EMPTY) {
        navigator.goTo<T>(keepState = keepState, withCustomAnimation = withCustomAnimation, arg = arg)
    }

    protected abstract val layoutResId: Int

    protected abstract fun injectDependecies()

    open fun getToolbarTitle(): String = emptyString
}