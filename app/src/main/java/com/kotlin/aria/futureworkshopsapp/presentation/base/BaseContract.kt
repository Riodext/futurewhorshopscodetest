package com.kotlin.aria.futureworkshopsapp.presentation.base

import com.kotlin.aria.basemvp.BaseMVPContract

/**
 * Created by aria on 07/12/2017.
 */
interface BaseContract {

    interface View: BaseMVPContract.View

    interface Presenter<V: BaseMVPContract.View> : BaseMVPContract.Presenter<V>
}