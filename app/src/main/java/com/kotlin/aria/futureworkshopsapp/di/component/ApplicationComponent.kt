package com.kotlin.aria.futureworkshopsapp.di.component

import com.kotlin.aria.futureworkshopsapp.di.module.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by aria on 06/12/2017.
 */
@Singleton
@Component(modules = [
    ApplicationModule::class,
    ApiModule::class,
    DomainModule::class,
    PresentationModule::class
])
interface ApplicationComponent {
       operator fun plus(activityModule: ActivityModule): ActivityComponent
}