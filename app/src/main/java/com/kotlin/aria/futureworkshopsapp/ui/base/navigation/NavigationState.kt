package com.kotlin.aria.futureworkshopsapp.presentation.navigation

/**
 * Created by aria on 09/12/2017.
 */
data class NavigationState constructor(
        var activeTag: String? = null,
        var firstTag: String? = null,
        var isCustomAnimationUsed: Boolean = false) {

    fun clear() {
        activeTag = null
        firstTag = null
    }
}