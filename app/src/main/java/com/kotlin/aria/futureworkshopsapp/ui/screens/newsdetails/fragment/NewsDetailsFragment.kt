package com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.fragment

import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseFragment
import com.kotlin.aria.futureworkshopsapp.presentation.base.ImageLoaderContract
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.model.PresentationNewsDetails
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.presenter.NewsDetailsPresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.contract.NewsDetailsContract
import com.kotlin.aria.futureworkshopsapp.ui.utils.constants.BundleConstants
import com.kotlin.aria.futureworkshopsapp.ui.utils.kotlinextensions.stringSeparator
import kotlinx.android.synthetic.main.fragment_news_details.*
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class NewsDetailsFragment: BaseFragment<NewsDetailsContract.View, NewsDetailsContract.Presenter>(),
        NewsDetailsContract.View {

    @Inject lateinit var newsDetailsPresenter: NewsDetailsPresenter

    @Inject lateinit var imageLoader: ImageLoaderContract

    override val layoutResId = R.layout.fragment_news_details

    override fun initPresenter() = newsDetailsPresenter

    override fun injectDependecies() {
        activityComponent.inject(this)
    }

    override fun onDetailsReceive(detail: PresentationNewsDetails) {
        imageLoader.load(activity, detail.imageUrl, iv_details_header)
        tv_details_title.text = detail.title
        tv_details_source_and_date.text = detail.source + stringSeparator + detail.date
        tv_details_content.text = detail.content
    }

    override fun showLoading() {
        // TODO add some fancy UI
    }

    override fun hideLoading() {
        // TODO
    }

    override fun showError() {
        // TODO
    }

    override fun initUI() {
        presenter.getNewsDetails(
                arguments.getString(BundleConstants.NEWS_ID))
    }

    override fun getToolbarTitle() =
            arguments.getString(BundleConstants.NEWS_TITLE)
}