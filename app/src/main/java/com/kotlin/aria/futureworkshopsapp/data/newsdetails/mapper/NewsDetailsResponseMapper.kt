package com.kotlin.aria.futureworkshopsapp.data.newsdetails.mapper

import com.kotlin.aria.futureworkshopsapp.data.newslist.model.NewsDetailsResponse
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity.DomainNewsDetails

/**
 * Created by aria on 28/12/2017.
 */
class NewsDetailsResponseMapper {

    //think doing this mapper in Java ;)
    fun translate(newsDetailsResponse: NewsDetailsResponse): DomainNewsDetails {
        return DomainNewsDetails(
                newsDetailsResponse.id,
                newsDetailsResponse.title,
                newsDetailsResponse.imageUrl,
                newsDetailsResponse.source,
                newsDetailsResponse.date,
                newsDetailsResponse.content)
    }
}