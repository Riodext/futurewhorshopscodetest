package com.kotlin.aria.futureworkshopsapp.presentation.navigation

import android.support.v4.app.Fragment

/**
 * Created by aria on 09/12/2017.
 */
data class Screen(val fragment: Fragment, val backStrategy: BackStrategy)