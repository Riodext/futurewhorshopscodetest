package com.kotlin.aria.futureworkshopsapp.presentation.newslist.presenter

import android.support.annotation.CallSuper
import com.kotlin.aria.futureworkshopsapp.domain.base.fetcher.result_listener.RequestType
import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews
import com.kotlin.aria.futureworkshopsapp.domain.search.interactor.NewsListInteractor
import com.kotlin.aria.futureworkshopsapp.presentation.base.ApiFetcherPresenter
import com.kotlin.aria.futureworkshopsapp.presentation.newslist.mapper.PresentationNewsMapper
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.contract.NewsListContract
import javax.inject.Inject

/**
 * Created by aria on 28/12/2017.
 */
class NewsListPresenter @Inject constructor(private val newsListInteractor: NewsListInteractor,
                                            private val mapper: PresentationNewsMapper)
    : ApiFetcherPresenter<NewsListContract.View>(), NewsListContract.Presenter {

    @CallSuper
    override fun onPresenterCreate() {
        super.onPresenterCreate()
        fetch(newsListInteractor.fetchNews(), RequestType.NEWS_LIST, onRequestSuccess())
    }

    private fun onRequestSuccess() = {
        it: List<DomainNews> ->
        view?.hideLoading()
        if (it.isNotEmpty()) {
            view?.hideNoItemsUI()
            view?.onItemsReceive(mapper.translate(it)) ?: Unit
        }
        else view?.showNoItemsUI() ?: Unit
    }

    override fun onRequestStart() {
        view?.showLoading()
    }

    override fun onRequestError(errorMessage: String?) {
        view?.hideLoading()
        view?.showError()
    }

}