package com.kotlin.aria.futureworkshopsapp.ui.screens.main.activity

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import com.kotlin.aria.futureworkshopsapp.R
import com.kotlin.aria.futureworkshopsapp.presentation.base.BaseActivity
import com.kotlin.aria.futureworkshopsapp.presentation.main.presenter.MainPresenter
import com.kotlin.aria.futureworkshopsapp.ui.screens.login.fragment.LoginFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.contract.MainActivityContract
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.fragment.NewsDetailsFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment
import com.kotlin.aria.futureworkshopsapp.ui.utils.preferences.PreferencesUtil
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity: BaseActivity<MainActivityContract.View, MainActivityContract.Presenter>(),
                     MainActivityContract.View {

    @Inject lateinit var mainPresenter: MainPresenter

    @Inject lateinit var preferences: PreferencesUtil

    override fun injectDependencies() = activityComponent.inject(this)

    override fun initPresenter() = mainPresenter

    override fun setUpFragment() {
        if (preferences.isUserLoggedIn) goTo<NewsListFragment>()
        else goTo<LoginFragment>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(main_toolbar)
        preferences.username.let {
            supportActionBar?.title = preferences.username } // if user is already logged
    }

    override fun onFragmentChanged(currentTag: String, currentFragment: Fragment) {
        presenter.onFragmentChanged(currentTag, currentFragment)
        enableBack(currentFragment)
    }

    override fun updateToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                navigator.goBack()
                return true
            }
            R.id.action_logout -> {
                presenter.logout()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun removeUser() {
        preferences.saveUserLogout()
    }

    override fun goToLoginScreen() {
        goTo<LoginFragment>()
    }

    private fun enableBack(currentFragment: Fragment) {
        if (currentFragment is NewsDetailsFragment) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }
}
