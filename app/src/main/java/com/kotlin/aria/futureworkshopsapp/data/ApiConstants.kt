package com.kotlin.aria.futureworkshopsapp.data

/**
 * Created by aria on 26/12/2017.
 */
object ApiConstants {

    const val BASE_ENDPOINT = "https://s3.amazonaws.com/future-workshops/"
}