package com.kotlin.aria.futureworkshopsapp.domain.search.interactor

import com.kotlin.aria.futureworkshopsapp.domain.search.entity.DomainNews
import com.kotlin.aria.futureworkshopsapp.domain.search.repository.NewsListRepository
import io.reactivex.Flowable

/**
 * Created by aria on 26/12/2017.
 */
class NewsListInteractor constructor(
        private val newsListRepository: NewsListRepository) {

    fun fetchNews(): Flowable<List<DomainNews>> {
        return newsListRepository.fetchNews()
    }
}