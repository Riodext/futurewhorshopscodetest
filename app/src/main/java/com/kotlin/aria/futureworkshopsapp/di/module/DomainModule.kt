package com.kotlin.aria.futureworkshopsapp.di.module

import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.interactor.NewsDetailsInteractor
import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.repository.NewsDetailsRepository
import com.kotlin.aria.futureworkshopsapp.domain.search.interactor.NewsListInteractor
import com.kotlin.aria.futureworkshopsapp.domain.search.repository.NewsListRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by aria on 18/12/2017.
 */
@Module
class DomainModule {

    @Singleton
    @Provides
    fun provideNewsListInteractor(repository: NewsListRepository): NewsListInteractor {
        return NewsListInteractor(repository)
    }

    @Singleton
    @Provides
    fun provideNewsDetailsInteractor(repository: NewsDetailsRepository): NewsDetailsInteractor {
        return NewsDetailsInteractor(repository)
    }

}
