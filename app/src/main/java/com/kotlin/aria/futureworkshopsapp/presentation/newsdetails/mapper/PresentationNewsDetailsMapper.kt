package com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.mapper

import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.entity.DomainNewsDetails
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.model.PresentationNewsDetails

/**
 * Created by aria on 28/12/2017.
 */
class PresentationNewsDetailsMapper {

    fun translate(domainNewsDetails: DomainNewsDetails): PresentationNewsDetails {
        return PresentationNewsDetails(
                domainNewsDetails.id,
                domainNewsDetails.title,
                domainNewsDetails.imageUrl,
                domainNewsDetails.source,
                domainNewsDetails.date,
                domainNewsDetails.content)
    }
}