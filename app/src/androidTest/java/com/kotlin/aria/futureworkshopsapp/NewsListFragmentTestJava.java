package com.kotlin.aria.futureworkshopsapp;

import android.support.test.espresso.contrib.RecyclerViewActions;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.activity.MainActivity;
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by aria on 01/01/2018.
 */

public class NewsListFragmentTestJava {

    @Rule
    public FragmentTestRule<MainActivity, NewsListFragment> fragmentTestRule =
            new FragmentTestRule<>(MainActivity.class, NewsListFragment.class);

    @Test
    public void clickButton() throws Exception {
        onView(withId(R.id.rv_news_list)).check(matches(isDisplayed()));

        onView(withId(R.id.rv_news_list)).perform(
                RecyclerViewActions.actionOnItemAtPosition(1, MyViewAction.clickChildViewWithId(R.id.tv_item_title)));

        onView(withId(R.id.rv_news_list)).check(matches(isDisplayed()));
    }


}
