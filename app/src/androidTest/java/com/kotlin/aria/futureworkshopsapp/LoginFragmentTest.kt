package com.kotlin.aria.futureworkshopsapp

import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.*
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.RootMatchers.withDecorView
import android.support.test.espresso.matcher.ViewMatchers.*
import com.android21buttons.fragmenttestrule.FragmentTestRule
import com.kotlin.aria.futureworkshopsapp.ui.screens.login.fragment.LoginFragment
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.activity.MainActivity
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.fail
import org.junit.Before
import org.junit.Rule
import org.junit.Test


/**
 * Created by aria on 31/12/2017.
 */
class LoginFragmentTest {

    private lateinit var myRule: FragmentTestRule<MainActivity, LoginFragment>

    @Rule
    fun fragmentTestRule(): FragmentTestRule<MainActivity, LoginFragment> {
        myRule = FragmentTestRule(MainActivity::class.java, LoginFragment::class.java)
        return myRule
    }

    @Before
    fun clearPreferences() {
        myRule.activity.deleteSharedPreferences(
                myRule.activity.getString(R.string.shared_preferences_name))
    }

    @Test
    fun toolbarTitle() {
        onView(withId(R.id.main_toolbar)).check(matches(isDisplayed()))
    }

    @Test
    fun userWantToLoginTypingUserName() {
        onView(withId(R.id.et_future_username_workshops)).perform(
                typeText("Fake testing user name"), closeSoftKeyboard())

        onView(withId(R.id.btn_login)).perform(click())

        if (myRule.activity.supportFragmentManager
                .findFragmentByTag(NewsListFragment::class.java.name) == null) {
            fail("Error - News List Fragment is not shown!")
        }
    }

    @Test
    fun userWantToLoginWithoutTypingUserName() {
        onView(withId(R.id.btn_login)).perform(click())
        onView(withText(R.string.minim_characters_error)).inRoot(
                withDecorView(not(myRule.activity.window.decorView)))
                .check(matches(isDisplayed()))
    }

}
