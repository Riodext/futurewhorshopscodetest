package com.kotlin.aria.futureworkshopsapp

import android.support.test.InstrumentationRegistry
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.contrib.RecyclerViewActions
import android.support.test.espresso.matcher.ViewMatchers.*
import android.support.test.runner.AndroidJUnit4
import android.support.v7.widget.RecyclerView
import com.android21buttons.fragmenttestrule.FragmentTestRule
import com.kotlin.aria.futureworkshopsapp.ui.screens.main.activity.MainActivity
import com.kotlin.aria.futureworkshopsapp.ui.screens.newslist.fragment.NewsListFragment
import org.hamcrest.Matchers.allOf
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Created by aria on 09/01/2018.
 */
@RunWith(AndroidJUnit4::class)
class NewsListFragmentTest {

    @get:Rule
    val fragmentRule: FragmentTestRule<MainActivity, NewsListFragment> =
            FragmentTestRule(MainActivity::class.java, NewsListFragment::class.java)

    @Test
    fun useAppContext() {
        val appContext = InstrumentationRegistry.getTargetContext()
        Assert.assertEquals("com.kotlin.aria.futureworkshopsapp", appContext.packageName)
    }

    @Test
    fun newsListIsVisible() {
        allOf(withParent(withId(R.id.rl_container)), withId(R.id.rv_news_list), isDisplayed())
    }

    @Test
    fun toolbarTitleIsVisible() {
        onView(withId(R.id.main_toolbar)).check(matches(isDisplayed()))
    }

    @Test
    fun clickButton() {
        allOf(withParent(withId(R.id.rl_container)), withId(R.id.rv_news_list), isDisplayed())

        onView(withId(R.id.rv_news_list)).perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                        1, MyViewAction.clickChildViewWithId(R.id.tv_item_title)))
    }

}