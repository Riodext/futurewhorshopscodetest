package com.kotlin.aria.futureworkshopsapp;

import com.kotlin.aria.futureworkshopsapp.domain.newsdetails.interactor.NewsDetailsInteractor;
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.mapper.PresentationNewsDetailsMapper;
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.model.PresentationNewsDetails;
import com.kotlin.aria.futureworkshopsapp.presentation.newsdetails.presenter.NewsDetailsPresenter;
import com.kotlin.aria.futureworkshopsapp.ui.screens.newsdetails.contract.NewsDetailsContract;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

/**
 * Created by aria on 29/12/2017.
 */

public class NewsDetailsPresenterTest {

    @Mock
    private NewsDetailsInteractor mInteractor;

    @Mock
    private PresentationNewsDetailsMapper mapper;

    private NewsDetailsPresenter mPresenter;

    @Before
    public void setUpPresenter() {
        MockitoAnnotations.initMocks(this);
        mPresenter = new NewsDetailsPresenter(mInteractor, mapper);
    }

    @Test
    public void loadNewsDetailsAndShowThem() {
        PresentationNewsDetails newsDetails = new PresentationNewsDetails(
                "100",
                "iPhone 8' to Come in Four Colors Including New 'Mirror-Like' Option",
                "https://cdn.macrumors.com/article-new/2017/07/DEOsfxlXcAA7RYl" +
                        ".jpg-large-800x600.jpeg",
                "macrumors",
                "Apple could make its upcoming OLED iPhone available in four different " +
                        "shades, including a new mirror-like reflective version not seen before in" +
                        " previous models. That's the latest claim from mobile leaker Benjamin" +
                        " Geskin, who shared an example image via Twitter over the weekend showing" +
                        " what the new color option could resemble.",
                "11/12/2016");

        mPresenter.getNewsDetails(newsDetails.getId());
        verify(mInteractor).fetchNewsDetails(newsDetails.getId());
    }
}
