package com.kotlin.aria.basemvp;

/**
 * Created by aria on 28/12/2017.
 */

public interface BaseLoadingContract {

    interface View extends BaseMVPContract.View {

        void showLoading();

        void hideLoading();

        void showError(String errorMessage);
    }

    interface Presenter extends BaseMVPContract.Presenter<View> {

    }

}
