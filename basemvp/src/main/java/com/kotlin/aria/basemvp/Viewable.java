package com.kotlin.aria.basemvp;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by aria on 28/12/2017.
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Viewable {

    int LAYOUT_NOT_DEFINED = -1;

    Class<? extends BaseMVPPresenter> presenter();

    int layout() default LAYOUT_NOT_DEFINED;
}
