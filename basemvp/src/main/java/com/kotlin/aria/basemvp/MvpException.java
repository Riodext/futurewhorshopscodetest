package com.kotlin.aria.basemvp;

/**
 * Created by aria on 28/12/2017.
 */

class MvpException extends RuntimeException {

    MvpException(String message) {
        super(message);
    }

    MvpException(String message, Throwable cause) {
        super(message, cause);
    }

    MvpException(Throwable cause) {
        super(cause);
    }
}
